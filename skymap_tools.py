import healpy as hp
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from xml.etree import ElementTree
import logging
import subprocess


def little_endian_bits(n):
    return list(map(int, [s for s in bin(n)[2:]]))


def quad_division(b):
    pos = [0, 0]
    for i in range(len(b)):
        pos[i % 2] += b[-(i + 1)] * 2 ** (i // 2)
    return pos


def position(n):
    return quad_division(little_endian_bits(n))


def face_to_square(nside, face):
    logging.info(f'nside={nside} face={face}')
    square = np.zeros(shape=(nside, nside), dtype=face.dtype)
    npix = nside ** 2
    coords = np.array(list(map(position, np.arange(npix))))
    for i in range(npix):
        square[coords[i, 0], coords[i, 1]] = face[i]
    return square


def skymap_to_faces(nside, skymap):
    faces = []
    for i_side in range(0, 12):
        i_low, i_high = (nside ** 2 * i_side, nside ** 2 * (i_side + 1))
        i_range_nested = np.arange(i_low, i_high)
        i_range_ring = hp.nest2ring(nside, i_range_nested)
        faces.append(skymap[i_range_ring])
    return faces


# skymap_to_squares = lambda n,m:map(lambda f:face_to_square(n,f), skymap_to_faces(n,m))
def skymap_to_squares(n, m):
    return [face_to_square(n, f) for f in skymap_to_faces(n, m)]


def encapsulate_in_svg(string, outfile):
    template_start = """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:cc="http://creativecommons.org/ns#"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns="http://www.w3.org/2000/svg"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
  xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="210mm"
   height="297mm"
   version="1.0"
   id="svg43"
   sodipodi:docname="Rhombicdodecahedron_net_stretched_pure_template.svg">
  <g
     inkscape:groupmode="layer"
     id="layer1"
     inkscape:label="map"
     style="stroke-linecap:butt;stroke-linejoin:round;stroke-miterlimit:100000">
       """
    outstring = template_start
    outstring += string
    outstring += """
  </g>
</svg>
"""
    with open(outfile, 'w') as f:
        f.write(outstring)


def convert_matshow_to_template(infile, **kwargs):
    r"""Convert the matplotlib output file at `fname` into a new svg image tag.
    Transforms the image entirely according to a template stored in `kwargs`,
     which has the keys id, x,y, width, height, transform (all as strings)."""

    tree = ElementTree.parse(infile)
    root = tree.getroot()
    image = None
    for image in root.iter('{http://www.w3.org/2000/svg}image'):
        break
    if image is None:
        raise ValueError('Did not find image in file %s' % infile)
    # kwargs["id"] = image.get('id')
    template_start = r"""
    <image
       transform="{transform}"
       id="{id}"
       """.format(**kwargs)
    template_end = r"""
       height="{height}"
       width="{width}"
       x="{x}"
       y="{y}"
       style="stroke-width:0.8408975;stroke-linecap:butt;stroke-linejoin:round;stroke-miterlimit:100000;image-rendering:optimizeSpeed"
    />""".format(**kwargs)

    out = ""
    out += template_start
    out += '       xlink:href="%s\n"' % image.get('{http://www.w3.org/1999/xlink}href')
    out += '       inkscape:label="#%s"' % image.get('id')
    out += template_end

    return out


# But on second thought that's all a lot of work...
# Instead I've just assembled an SVG once, then I can take the matrices and coordinates
# assuming the image objects have the expected IDs image1...image12
def parse_template_file(infile):
    """Parses an SVG at `infile` which is expected to contain a set of images"""

    tree = ElementTree.parse(infile)

    root = tree.getroot()

    template = {}
    for image in root.iter('{http://www.w3.org/2000/svg}image'):

        image_id = image.get('id')
        prefix = 'image'
        if image_id.startswith(prefix):
            image_id = image_id[len(prefix):]
        if image_id.isdigit():
            image_id = int(image_id)
        template[image_id] = {}
        for key in ['id', 'x', 'y', 'width', 'height', 'transform']:
            template[image_id][key] = image.get(key)
    return template


def pretty_skymap(infile):
    skymap = np.load(infile)
    # interpolate to finer map
    nside = 512
    pix = hp.pixelfunc.nside2npix(nside)
    # create coordinate grid
    # NOTE: theta is NOT in local coordinates, but equatorial
    theta, ra = hp.pixelfunc.pix2ang(nside, np.arange(pix))
    # turn from zenith to declination
    dec = np.pi / 2 - theta
    # get previous scans and interpolate to nside
    skymap_interp = hp.pixelfunc.get_interp_val(skymap, theta, ra)
    del skymap
    # Smooth with a small kernel
    skymap_smooth = hp.smoothing(skymap_interp, fwhm=np.radians(.2))
    del skymap_interp
    return skymap_smooth


def turn_map_into_image_tags(skymap, template, cmap='viridis', vmin=None, vmax=None):
    image_tags = ""
    nside = hp.npix2nside(skymap.size)
    squares = skymap_to_squares(nside, skymap)
    for i, s in enumerate(squares):
        plt_file = '_temp_healpix_squares_%i.svg' % i
        plt.imshow(s,
                   cmap=cmap, vmin=vmin, vmax=vmax,
                   origin='lower')  # newer MPL has default origin=upper!
        plt.savefig(plt_file, dpi=240)
        template_i = template[i + 1]
        image_tags += convert_matshow_to_template(plt_file, **template_i)
        subprocess.call(['rm', plt_file])
    return image_tags


ps_map = {'blue': ((0.0, 0.0, 1.0),
                   (0.05, 1.0, 1.0),
                   (0.4, 1.0, 1.0),
                   (0.6, 1.0, 1.0),
                   (0.7, 0.2, 0.2),
                   (1.0, 0.0, 0.0)),
          'green': ((0.0, 0.0, 1.0),
                    (0.05, 1.0, 1.0),
                    (0.5, 0.0416, 0.0416),
                    (0.6, 0.0, 0.0),
                    (0.8, 0.5, 0.5),
                    (1.0, 1.0, 1.0)),
          'red': ((0.0, 0.0, 1.0),
                  (0.05, 1.0, 1.0),
                  (0.5, 0.0416, 0.0416),
                  (0.6, 0.0416, 0.0416),
                  (0.7, 1.0, 1.0),
                  (1.0, 1.0, 1.0))}

ps_map = matplotlib.colors.LinearSegmentedColormap('ps_map', ps_map, 256)
