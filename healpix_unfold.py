#!/usr/bin/env python


import pickle
import argparse
from skymap_tools import *
import logging
logging.basicConfig(level='WARN')

parser = argparse.ArgumentParser('Turn a healpix map into a paper model SVG')
parser.add_argument('skymap_file', type=str)
parser.add_argument('output_file', type=str)

if __name__ == '__main__':
    args = parser.parse_args()
    logging.info(args)

    with open('template/template_a4.pkl', mode='rb') as f:
        template = pickle.load(f)

    logging.info(list(template.keys()))
    logging.info(template[1])
    # sys.exit()

    logging.info(f'{args.skymap_file} {args.output_file}')
    skymap = np.load(args.skymap_file)
    # skymap = np.arange(hp.nside2npix(2**5))
    # skymap = hp.pix2ang(2**5, np.arange(hp.nside2npix(2**5)), lonlat=True)[0]
    image_tags = turn_map_into_image_tags(skymap, template, vmin=0, vmax=skymap.max(), cmap='Greens')

    encapsulate_in_svg(image_tags, args.output_file)
    subprocess.call(['inkscape', '--export-filename=%s.pdf' % args.output_file, args.output_file])
